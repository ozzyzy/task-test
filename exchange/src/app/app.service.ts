import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class AppService {
  apiUrl = 'https://openexchangerates.org/api/latest.json?app_id=174f0eff7c474c8daaae3f2abb961176&base=USD';

  constructor(private http: HttpClient) {
  }

  public get() {
  return this.http.get(this.apiUrl);
  }

}
