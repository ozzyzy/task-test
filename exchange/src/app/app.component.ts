import { Component, OnInit } from '@angular/core';
import {AppService} from "./app.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private service: AppService ) {}

  ngOnInit() {
   const getCurrentValues = () => {
      this.service.get()
        .subscribe((data: any) => {
          let currencies = createСurrencies(data.rates);    //got needed currencies list
          let totalPrice = selectedPrice(selectedCart); //got started sum
          multiplyValues(currencies, totalPrice);        //multiplied started sum and currencies
          createObject(currencies, totalCartPrice);      //created final object
          console.log(totalCartPrice);
        });
    };

    const createСurrencies = (d) => ({
        rubles: d.RUB,
        euros: d.EUR,
        dollars: d.USD,
        pounds: d.GBP,
        yens: d.JPY,
    });

    const selectedCart = [
      { price: 20 },
      { price: 45 },
      { price: 67 },
      { price: 1305 }
    ];

    const totalCartPrice = {
      rubles: 0,
      euros: 0,
      dollars: 0,
      pounds: 0,
      yens: 0,
    };

    const sum = (a, b) => a + b;
    const mult = (a, b) => a * b;

    const selectedPrice = (arr) => arr.reduce((final, cv) => sum(final, cv.price), 0);

    const multiplyValues = (obj, num) => {
      Object.keys(obj).forEach(key => {
        obj[key] = mult(obj[key], num);
      });
    };

    const setKey = (obj, key, value) => {
      obj[key] = value;
    };

    const createObject = (obj1, obj2) => {
      Object.keys(obj1).forEach(key => {
        setKey(obj2, key, obj1[key]);
      })
    };

    getCurrentValues();
  }

}



